from django import forms
from django.contrib.auth.models import User


class SignUpForm(forms.ModelForm):
    email = forms.EmailField(max_length=100)
    password = forms.CharField(max_length=100, widget=forms.PasswordInput(attrs={'class': 'form-control'}))
    repeat_password = forms.CharField(max_length=200, widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ['email', 'password', 'repeat_password', ]

    def clean_repeat_password(self):
        password = self.cleaned_data.get("password")
        repeat_password = self.cleaned_data.get("repeat_password")

        if password and repeat_password and password != repeat_password:
            raise forms.ValidationError("Password don't match.")

        return repeat_password

    def save(self):
        super().clean()

        return User.objects.create_user(
            username=self.cleaned_data.get('email'),
            email=self.cleaned_data.get('email'),
            password=self.cleaned_data.get('password'))