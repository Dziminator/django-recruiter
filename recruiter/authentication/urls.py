from django.conf.urls import url

from . import views

app_name = 'authentication'
urlpatterns = [
    url(r'^login/$', views.sign_in, name='login'),
    url(r'^logout/?$', views.logout_view, name='logout'),
    url(r'^signup/?$', views.sign_up, name='signup')
]