from django.shortcuts import render, redirect
from django.core.urlresolvers import reverse
from django.contrib.auth import authenticate, login, logout

from .forms import SignUpForm


def sign_in(request):
    if request.method == 'POST':
        username = request.POST.get('userName')
        password = request.POST.get('password')

        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            next_url = request.GET.get('next')
            return redirect(next_url if next_url is not None else reverse('recruiter:home'))
        else:
            auth_error = "Wrong credentials. User hasn't been found"
            return render(request, 'authentication/login.html', {'auth_error': auth_error})
    else:
        return render(request, 'authentication/login.html')


def sign_up(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)

        if form.is_valid():
            user = form.save()
            if user is not None:
                login(request, user)
            return redirect('/')
    else:
        form = SignUpForm()

    return render(request, 'authentication/signup.html', {'form': form})


def logout_view(request):
    if request.user.is_authenticated:
        logout(request)

    return redirect(reverse('recruiter:home'))
