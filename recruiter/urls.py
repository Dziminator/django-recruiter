from django.conf.urls import url, include
from django.contrib import admin
from recruiter.core import views as core_views

urlpatterns = [
    url(r'^$', core_views.home, name='home'),
    url(r'', include('recruiter.authentication.urls')),
    url(r'^admin/', admin.site.urls)
]